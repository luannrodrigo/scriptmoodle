const fetch = require('node-fetch')

const url = 'https://api.codenation.dev/v1/challenge/dev-ps/generate-data?token=d034e569df6652977bbd7b3be5f8396228c5c254';
const alfabeto = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
var numeroCasa = 0;
//bx uxwp jwm cqjwtb oxa juu cqn orbq! mxdpujb jmjvb

// Enumera posição da Letra
const converteCodigo = function(letra) {
    let codigo = alfabeto.indexOf(letra.toUpperCase());
    return codigo;
}

const descriptografar = function(e){
    if(e === -1)    
        return ' ';
    else{
        let num = parseInt(e) - numeroCasa;
        if(num < 0){
            num = num * -1;
            let a =  alfabeto.length -( 0 - num);
            console.log(a);
        }
        else{
            return alfabeto[e - numeroCasa];
        }
    }
};
fetch(url)
    .then((res) => res.json())
    .then((json) => {
        let cifrado = Array.from(json.cifrado);
        numeroCasa = json.numero_casas;
        let descrifado = cifrado.map(converteCodigo).map(descriptografar);
        console.log(descrifado); return false;
        console.log(descrifado);
    });

// const urlPost = 'http//httpbin.org/post'

// fetch(urlPost, {})
//     .then((res) => res.json())
//     .then((json) => console.log(json))
//     .catch((e) => console.log(e))

